<?php
include 'postApi.php';

$errors = array(
    '400' => 'Неверные параметры в запросе',
    '500' => 'Ошибка API',
);
$error = null;

if(isset($_GET['m'])){
    $code = $_GET['m'];
} else {
    $error = 400;
}

if(preg_match('@[^a-z0-9]+@i', $code)){
    $error = 400;
}

if(strlen($code) < 10 OR strlen($code) > 20){
    $error = 400;
}

if($error !== null){
    $json = new stdClass();
    $json->status = $error;
    $json->message = $errors[$error];
    header('Content-Type: application/json; charset=utf-8');
    print(json_encode($json, JSON_UNESCAPED_UNICODE));
    die();
}

$api = new postApi();
$json = $api->getData($code);
if(!isset($json->statuses)){
    $json->statuses->timestamp = time();
    $json->statuses->parcel_status_id = 0;
    $json->statuses->message = 'Посылка не найдена';

}


header('Content-Type: application/json; charset=utf-8');
print(json_encode($json, JSON_UNESCAPED_UNICODE));
die();