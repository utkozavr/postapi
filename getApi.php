<?php

function connectDb()
{

    $host = 'localhost';
    $user = 'root';
    $password = "123";
    $dataBase = 'post';

    $conn = new mysqli($host, $user, $password, $dataBase);
    $conn->set_charset("utf8");
    if ($conn->connect_error) {
        die('Connect Error (' . $conn->connect_errno . ') '
            . $conn->connect_error);
    }
    return $conn;
}

function getTrack($code, $conn)
{
    $id = null;
    $done = null;

    $query = $conn->prepare("SELECT `id`, `done_flag` FROM `track` WHERE `code` = ?");
    $query->bind_param("s", $code);
    $query->execute();
    $query->bind_result($id, $done);
    $query->fetch();
    $query->close();

    return array('id' => $id, 'done' => $done);
}

function setTrackDone($track, $conn)
{
    $query = $conn->prepare("UPDATE `track` SET `done_flag` = 1 WHERE `id` = ?");
    $query->bind_param("i", $track['id']);
    $query->execute();
    $query->close();
}

function getStateByCode($code, $conn)
{
    $id = null;
    $message = null;
    $query = $conn->prepare("SELECT `id`, `message` FROM `state` WHERE `code` = ?");
    $query->bind_param("i", $code);
    $query->execute();
    $query->bind_result($id, $message);
    $query->fetch();
    $query->close();
    return array('id' => $id, 'message' => $message);
}

function setState($code, $message, $conn)
{
    $query = $conn->prepare("INSERT INTO `state` (`message`, `code`) VALUES (?, ?)");
    $query->bind_param('si', $message, $code);
    $query->execute();
    $query->close();
    $id = mysqli_insert_id($conn);
    return $id;
}

function setStatus($trackId, $stateId, $timeStump, $conn)
{
    $query = $conn->prepare("INSERT INTO `status`(`track_id`, `state_id`, `status_date`) VALUES (?, ?, ?)");
    $query->bind_param("iis", $trackId, $stateId, $timeStump);
    $query->execute();
    $query->close();
    $id = mysqli_insert_id($conn);
    return $id;
}

function setStatusFromApi($json, $track, $conn)
{
    $lastSate = 4;

    $status = $json->status;
    $timestamp = $json->timestamp;
    $parcel_status_id = $json->parcel_status_id;
    $message = $json->message;

    if ($status == 200) {
        $state = getStateByCode($parcel_status_id, $conn);
        if ($state['id'] == null) {
            $state['id'] = setState($parcel_status_id, $message, $conn);
        }
        setStatus($track['id'], $state['id'], $timestamp, $conn);
    }

    if ($parcel_status_id == $lastSate) {
        setTrackDone($track, $conn);
    }
}


function curl($code)
{
    $token = 'f7b08bd736b26c752a73aa363e79720e';
    $url = 'http://api.mevixtest.ru/statuses/';
    $timeOutMs = 4000;
    $result = null;
    $url = $url . $token . '/' . $code . '.json';
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_HEADER, false);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_TIMEOUT_MS, $timeOutMs);
    $result = curl_exec($c);
    curl_close($c);
    return $result;
}

$code = $argv[1];

$conn = connectDb();
$track = getTrack($code, $conn);
$response = curl($code, $conn);
if ($response !== false) {
    $obj = json_decode($response);
    setStatusFromApi($obj, $track, $conn);
}

$conn->close();





