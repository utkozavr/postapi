<?php

class postApi
{
    private $host = 'localhost';
    private $user = 'root';
    private $password = "123";
    private $dataBase = 'post';
    private $conn;

    public function connectDb(){
        $this->conn = new mysqli($this->host, $this->user, $this->password, $this->dataBase);
        $this->conn->set_charset("utf8");
        if ($this->conn->connect_error) {
            die('Connect Error (' . $this->conn->connect_errno . ') '
                . $this->conn->connect_error);
        }
    }

    private function setTrack($code){
        $query = $this->conn->prepare("INSERT INTO `track`(`code`, `done_flag`) VALUES (?, 0)");
        $query->bind_param("s", $code);
        $query->execute();
        $query->close();
        $id = mysqli_insert_id($this->conn);

        return $id;
    }

    private function getTrack($code){
        $id = null;
        $done = null;

        $query = $this->conn->prepare("SELECT `id`, `done_flag` FROM `track` WHERE `code` = ?");
        $query->bind_param("s", $code);
        $query->execute();
        $query->bind_result($id, $done);
        $query->fetch();
        $query->close();

        return array('id' => $id, 'done' => $done);
    }

    private function getStatusByTrack($track)
    {
        $rows = array();
        $query = 'SELECT `status`.`status_date`, `state`.`code`, `state`.`message`
        FROM `status`, `state`
        WHERE `status`.`state_id` = `state`.`id`
        AND `status`.`track_id` = ' . $track['id'];

        $result = $this->conn->query($query);
        while ($row = $result->fetch_row()) {
            $rows[] = array($row[0], $row[1], $row[2]);
        }

        return $rows;
    }

    private function toJSON($trackCode, $data)
    {
        $obj = new stdClass();
        $obj->trackid = $trackCode;

        foreach($data as $row){
            $obj->statuses->timestamp = $row[0];
            $obj->statuses->parcel_status_id = (int) $row[1];
            $obj->statuses->message = $row[2];
        }
        return $obj;
    }

    public function getData($code)
    {
        $data = array();
        $this->connectDb();
        $track = $this->getTrack($code);

        if($track['id'] == 0){
            $track['id'] = $this->setTrack($code);
            $track['done'] = 0;
        }

        if($track['done'] == 1){
            $data = $this->getStatusByTrack($track);
        } else {
            exec('nohup php getApi.php ' . $code . ' > /dev/null &');
            $data = $this->getStatusByTrack($track);
        }
        $this->conn->close();
        $jsonObj = $this->toJSON($code, $data);

        return $jsonObj;
    }



} 